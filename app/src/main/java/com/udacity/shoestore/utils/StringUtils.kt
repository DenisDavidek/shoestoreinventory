package com.udacity.shoestore.utils

class StringUtils {

    companion object {

        fun checkEnteredShoeData(name: String, size: String, company: String, description: String): Boolean {

            return name.isNotEmpty() && size.isNotEmpty() && company.isNotEmpty() && description.isNotEmpty()
        }
    }
}
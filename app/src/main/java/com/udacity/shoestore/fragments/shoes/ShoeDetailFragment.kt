package com.udacity.shoestore.fragments.shoes

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.udacity.shoestore.R
import com.udacity.shoestore.databinding.FragmentShoeDetailBinding
import com.udacity.shoestore.models.Shoe
import com.udacity.shoestore.utils.StringUtils
import kotlinx.android.synthetic.main.fragment_shoe_detail.*


class ShoeDetailFragment : Fragment() {

    private lateinit var binding: FragmentShoeDetailBinding

    private lateinit var shoesViewModel: ShoesViewModel

    private val args: ShoeDetailFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shoe_detail, container, false)
        binding.lifecycleOwner = this

        shoesViewModel = ViewModelProvider(requireActivity()).get(ShoesViewModel::class.java)

        /** I was not sure, if I had to do something with the save button after user clicked on the shoe inside ShoesFragment.
         *  I thought about a lot of different options(for example hide save button), but I have decided not to change the save shoe button, when user clicked on the shoe inside ShoesFragment
         *  When you try to edit selected shoe and save that shoe, then the data will be saved as a new object into shoesList which is located in ShoesViewModel.
         *
         *  I chose that option because for example: when you want to add the same shoe but with different sizes, it would be a lot more typing,
         *  than the current option which is: just select shoe, edit parameters you want and save. This will be the quickest way to add
         *  different sizes of the same shoe.
         * */
        binding.btnSaveShoe.setOnClickListener {


            if (StringUtils.checkEnteredShoeData(edt_shoe_name.text.toString(), edt_shoe_size.text.toString(), edt_shoe_company.text.toString(), edt_shoe_description.text.toString())) {

                val shoe = Shoe(edt_shoe_name.text.toString(), edt_shoe_size.text.toString().toDouble(), edt_shoe_company.text.toString(), edt_shoe_description.text.toString())

                shoesViewModel.addShoe(shoe)
                findNavController().navigate(ShoeDetailFragmentDirections.actionShoeDetailToShoes())

            } else Toast.makeText(requireContext(), getString(R.string.not_all_fields_filled), Toast.LENGTH_SHORT).show()


        }

        binding.btnCancel.setOnClickListener {
            findNavController().navigate(ShoeDetailFragmentDirections.actionShoeDetailToShoes())
        }

        if (args.shoe != null) {
            binding.shoe = args.shoe
        }
        return binding.root
    }

}
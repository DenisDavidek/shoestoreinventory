package com.udacity.shoestore.fragments.shoes

import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.udacity.shoestore.R
import com.udacity.shoestore.databinding.FragmentShoesBinding
import com.udacity.shoestore.databinding.ShoeItemBinding
import com.udacity.shoestore.models.Shoe


class ShoesFragment : Fragment() {


    private lateinit var binding: FragmentShoesBinding

    private lateinit var viewModel: ShoesViewModel

    private lateinit var shoeItemBinding: ShoeItemBinding


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shoes, container, false)
        binding.lifecycleOwner = this

        viewModel = ViewModelProvider(requireActivity()).get(ShoesViewModel::class.java)

        binding.fabAddShoe.setOnClickListener {
            findNavController().navigate(ShoesFragmentDirections.actionShoeToShoeDetail())
        }

        viewModel.shoesList.observe(viewLifecycleOwner, Observer { shoes ->
            displayShoes(shoes)
        })


        displayShoes(viewModel.shoesList.value!!)

        setHasOptionsMenu(true)
        return binding.root
    }

    private fun displayShoes(shoesList: MutableList<Shoe>) {
        binding.llShoes.removeAllViews()
        for (index in 0 until shoesList.size) {

            shoeItemBinding = DataBindingUtil.inflate(layoutInflater, R.layout.shoe_item, binding.llShoes, true)
            shoeItemBinding.shoe = viewModel.shoesList.value?.get(index)
            shoeItemBinding.clShoeParent.setOnClickListener {
                findNavController().navigate(ShoesFragmentDirections.actionShoeToShoeDetail(shoeItemBinding.shoe))
            }

        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.logout_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.action_logout -> {

                findNavController().navigate(ShoesFragmentDirections.actionShoesToLogin())

            }
        }

        return super.onOptionsItemSelected(item)
    }

}
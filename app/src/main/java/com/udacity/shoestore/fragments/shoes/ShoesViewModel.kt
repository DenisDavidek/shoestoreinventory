package com.udacity.shoestore.fragments.shoes

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.udacity.shoestore.models.Shoe

class ShoesViewModel : ViewModel() {

    private val _shoesList = MutableLiveData<MutableList<Shoe>>(mutableListOf())
    val shoesList: LiveData<MutableList<Shoe>>
        get() = _shoesList


    init {

    }

    fun addShoe(shoe: Shoe) {
        _shoesList.value?.add(shoe)
    }

    override fun onCleared() {
        super.onCleared()
    }


}